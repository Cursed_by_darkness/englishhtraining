package com.example.englishtraining.dictionary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.englishtraining.DatabaseHelper;
import com.example.englishtraining.MainActivity;
import com.example.englishtraining.R;
import com.example.englishtraining.task.SectionsActivity;
import com.example.englishtraining.task.TaskCategoriesActivity;

import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainDictionaryActivity extends AppCompatActivity implements View.OnClickListener {

    //Переменная для работы с БД
    private DatabaseHelper mDBHelper;
    private SQLiteDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dictionary);

        mDBHelper = new DatabaseHelper(this);

        try {
            mDBHelper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            mDb = mDBHelper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }

        ListView listView = (ListView)findViewById(R.id.categories_list);
        CategoriesAdapter categoriesAdapter = new CategoriesAdapter();

        listView.setAdapter(categoriesAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainDictionaryActivity.this, WordsListActivity.class);
                intent.putExtra("Position", position);
                startActivity(intent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                TextView word = (TextView)findViewById(R.id.item_text);
                deleteRow(word.getText().toString());
                categoriesAdapter.reloadDatabase();
                categoriesAdapter.notifyDataSetChanged();
                return true;
            }
        });

        Button add_button = (Button)findViewById(R.id.add_button);
        add_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, AddCategoryActivity.class);
        startActivity(intent);
    }

    public void deleteRow(String name){
        mDb.delete("Categories", "Name = \"" + name + "\"", null);

    }

    public List<String> getNames(){
        List<String> words = new ArrayList<String>();

        Cursor cursor = mDb.rawQuery("SELECT Name FROM Categories;", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            words.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();

        return words;
    }

    class CategoriesAdapter extends BaseAdapter{

        List<String> CATEGORIES = getNames();

        @Override
        public int getCount() {
            return CATEGORIES.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public void reloadDatabase(){
            CATEGORIES = getNames();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.custom_category_item, null);

            TextView textView =(TextView)view.findViewById(R.id.item_text);
            textView.setText(CATEGORIES.get(position));

            return view;
        }
    }
}