package com.example.englishtraining.task;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.englishtraining.R;

public class VocabularyActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vocabulary);
        Button first_variant = (Button)findViewById(R.id.first_variant);
        Button second_variant = (Button)findViewById(R.id.second_variant);
        Button third_variant = (Button)findViewById(R.id.third_variant);
        Button fourth_variant = (Button)findViewById(R.id.fourth_variant);
        first_variant.setOnClickListener(this);
        second_variant.setOnClickListener(this);
        third_variant.setOnClickListener(this);
        fourth_variant.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, ResultActivity.class);
        startActivity(intent);
    }
}