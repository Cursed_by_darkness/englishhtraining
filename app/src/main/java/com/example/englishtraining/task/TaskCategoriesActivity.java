package com.example.englishtraining.task;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.englishtraining.R;
import com.example.englishtraining.dictionary.MainDictionaryActivity;

public class TaskCategoriesActivity extends AppCompatActivity {

    String[] CATEGORIES = {"Cat1", "Cat2", "Cat3"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        ListView listView = (ListView)findViewById(R.id.categories_list);
        TaskCategoriesActivity.CategoriesAdapter categoriesAdapter = new TaskCategoriesActivity.CategoriesAdapter();

        listView.setAdapter(categoriesAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(TaskCategoriesActivity.this, TaskInputWordsTypeActivity.class);
                startActivity(intent);
            }
        });
    }

    class CategoriesAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return CATEGORIES.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.custom_category_item, null);

            TextView textView =(TextView)view.findViewById(R.id.item_text);
            ImageView imageView =(ImageView) view.findViewById(R.id.item_background);
            textView.setText(CATEGORIES[position]);
            imageView.setImageResource(R.drawable.ic_blue_list_item);

            return view;
        }
    }
}