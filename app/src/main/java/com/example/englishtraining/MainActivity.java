package com.example.englishtraining;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.example.englishtraining.dictionary.MainDictionaryActivity;
import com.example.englishtraining.map.MainMapActivity;
import com.example.englishtraining.task.MainTaskActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button dictionary_button = findViewById(R.id.dictionary_button);
        Button task_button = findViewById(R.id.task_button);
        Button map_button = findViewById(R.id.map_button);
        dictionary_button.setOnClickListener(this);
        task_button.setOnClickListener(this);
        map_button.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId())
        {
            case R.id.dictionary_button:
                intent = new Intent(this, MainDictionaryActivity.class);
                startActivity(intent);
                break;
            case R.id.task_button:
                intent = new Intent(this, MainTaskActivity.class);
                startActivity(intent);
                break;
            case R.id.map_button:
                intent = new Intent(this, MainMapActivity.class);
                startActivity(intent);
                break;
        }
    }
}